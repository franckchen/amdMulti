define(function (require) {
    var core = require('./core');

    core.on('asyncOperation1Finished', function () {
        console.log('异步事件1完成日志打点');
    });

    core.on('asyncOperation2Finished', function () {
        console.log('异步事件2完成日志打点');
    });
});
