define(function (require) {
    console.log('这是移动特有的代码');

    var core = require('./core');

    core.on('asyncOperation1Finished', function () {
        console.log('按键1移动特有的附加功能');
    });

    core.on('asyncOperation2Finished', function () {
        console.log('按键2移动特有的附加功能');
    });
});
