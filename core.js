define(function (require) {
    var Emitter = require('./EventEmitter');

    var core = {};

    Emitter.mixin(core);

    var button1 = document.getElementById('btn1');
    var button2 = document.getElementById('btn2');

    button1.addEventListener('click', function () {
        console.log('按键1被点击');

        setTimeout(
            function () {
                core.emit('asyncOperation1Finished');
            },
            2000
        );
    });

    button2.addEventListener('click', function () {
        console.log('按键2被点击');

        setTimeout(
            function () {
                core.emit('asyncOperation2Finished');
            },
            2000
        );
    });

    return core;
});
