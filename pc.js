define(function (require) {
    console.log('这是pc特有的代码');

    var core = require('./core');

    core.on('asyncOperation1Finished', function () {
        console.log('按键1pc特有的附加功能');
    });

    core.on('asyncOperation2Finished', function () {
        console.log('按键2pc特有的附加功能');
    });
});
